import { Session } from '@nestjs/common';
import { Get } from '@nestjs/common';
import { Controller } from '@nestjs/common';

@Controller('session')
export class SessionController {
  @Get()
  findAll(@Session() session: Record<string, any>) {
    return (session.visits = session.visits ? session.visits + 1 : 1);
  }
}
