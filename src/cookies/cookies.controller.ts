import { Get, Response, Request, Param } from '@nestjs/common';
import { Controller } from '@nestjs/common';

@Controller('cookies')
export class CookiesController {
  @Get('setcookies')
  SetCookie(@Response({ passthrough: true }) response) {
    response.cookie('token', '1234', {
      maxAge: 1000 * 60 * 10,
      httpOnly: true,
    });
    return 'Cookies set';
  }

  @Get('getcookies')
  getCookies(@Request() request) {
    return { ...request.cookies };
  }

  @Get('getcookies/:id')
  getCookie(@Request() request, @Param() params) {
    return request.cookies[params.id];
  }
}
