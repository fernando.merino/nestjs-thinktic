import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SessionController } from './session/session.controller';
import { CookiesController } from './cookies/cookies.controller';
import { AuthModule } from './auth/auth.module';
import { UserController } from './user/user.controller';
// import { GraphQLModule } from '@nestjs/graphql';
import { join } from 'path';

@Module({
  imports: [
    AuthModule,
    
    // GraphQL rompe la aplicación por un problema de dependencias
    // Parece estar relacionado con que GraphQL requiere NestJS 8
    // pero NestJS 8 no parece estar aun disponible.

    // GraphQLModule.forRoot({
    //   autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
    // }),
  ],
  controllers: [
    AppController,
    SessionController,
    CookiesController,
    UserController,
  ],
  providers: [AppService],
})
export class AppModule {}
