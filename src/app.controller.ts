import {
  Controller,
  Get,
  Request,
  Post,
  UseGuards,
  Session,
} from '@nestjs/common';
import { AppService } from './app.service';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth/auth/auth.service';
import { AuthenticatedGuard } from './auth/authenticated.guard';
import { LoginGuard } from './auth/login.guard';
import { Role } from './auth/role.enum';
import { Roles } from './auth/roles.decorator';
import { createCipheriv, createDecipheriv, randomBytes, scrypt } from 'crypto';
import { promisify } from 'util';
import * as bcrypt from 'bcrypt';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly authService: AuthService,
  ) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
  getHelloService() {
    return this.appService.getHello();
  }

  @UseGuards(AuthGuard('local'))
  @UseGuards(LoginGuard)
  @Post('auth/login')
  async login(@Request() req, @Session() session: Record<string, any>) {
    const response = await this.authService.login(req.user);
    session.user = response.payload;
    return { access_token: response.access_token };
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('profile')
  getProfile(@Request() req) {
    return req.user;
  }

  @Get('miguard')
  @UseGuards(AuthenticatedGuard)
  getGuard() {
    return 'logueado';
  }

  @Get('roles')
  @UseGuards(AuthGuard('jwt'))
  @UseGuards(AuthenticatedGuard)
  @Roles(Role.Admin)
  getRole(@Request() req) {
    return req.user.roles;
  }

  @Get('cifrado')
  async cifrado() {
    const iv = randomBytes(16);
    const password = 'Password used to generate key';
    const key = (await promisify(scrypt)(password, 'salt', 32)) as Buffer;
    const cipher = createCipheriv('aes-256-ctr', key, iv);

    const textToEncrypt = 'Nest';
    const encryptedText = Buffer.concat([
      cipher.update(textToEncrypt),
      cipher.final(),
    ]);

    const decipher = createDecipheriv('aes-256-ctr', key, iv);
    const decryptedText = Buffer.concat([
      decipher.update(encryptedText),
      decipher.final(),
    ]);

    return {
      textToCifer: textToEncrypt,
      encryptedText: encryptedText.toString(),
      decryptedText: decryptedText.toString(),
    };
  }

  @Get('hash') async hash() {
    const saltOrRounds = 10;
    const password = 'random_password';
    const hash = await bcrypt.hash(password, saltOrRounds);
    const salt = await bcrypt.genSalt();
    const isMatch = await bcrypt.compare(password, hash);
    return { hash: hash, salt: salt, isMatch: isMatch };
  }
}
