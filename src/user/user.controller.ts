import { Controller, Get, Post, Param, Body, Query } from '@nestjs/common';

@Controller('user')
export class UserController {
  @Get()
  getUsers(): string {
    return 'get /user';
  }

  @Get(':id')
  getUser(@Param() params): string {
    return 'get /user/:id ' + params.id;
  }

  @Post()
  postUser(): string {
    return 'post /user';
  }

  @Post('data')
  postData(@Query() query): string {
    return `post /user ${query.username} ${query.password}`;
  }

  @Post('dto')
  postDto(@Body() body): string {
    return 'This action adds a new object with name: ' + body.name;
  }
}
