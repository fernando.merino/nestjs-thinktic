import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as session from 'express-session';
import * as cookieParser from 'cookie-parser';
import * as passport from 'passport';
import flash = require('connect-flash');
import * as helmet from 'helmet';
import * as csurf from 'csurf';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(
    session({
      secret: 'my-secret',
      resave: false,
      saveUninitialized: false,
    }),
  );
  app.use(cookieParser());
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(flash());
  app.use(helmet());
  app.enableCors();
  app.use(csurf());

  // El módulo de Swagger me rompla la aplicación al tratar de crear el documento
  // Parece que no detecta bien los rutas dela app, pero no encuentro documentación del error para resolverlo
  // Si implemento Swagger en un proyecto de NestJS desde cero si me gunciona
  // No sé si hay algún conflicto con toda la configuración de seguridad creada anteriormente
  
  // const config = new DocumentBuilder()
  //   .setTitle('Cats example')
  //   .setDescription('The cats API description')
  //   .setVersion('1.0')
  //   .addTag('cats')
  //   .build();
  // const document = SwaggerModule.createDocument(app, config);
  // SwaggerModule.setup('api', app, document);

  await app.listen(3000);
}
bootstrap();
